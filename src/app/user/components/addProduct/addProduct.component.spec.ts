import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { addProductComponent } from './addProduct.component';

describe('CategoryComponent', () => {
  let component: addProductComponent;
  let fixture: ComponentFixture<addProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ addProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(addProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
