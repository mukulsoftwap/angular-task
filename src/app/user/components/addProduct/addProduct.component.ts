import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../user.service';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';

@Component({
  selector: 'app-addProduct',
  templateUrl: './addProduct.component.html',
  styleUrls: ['./addProduct.component.css']
})
export class addProductComponent implements OnInit {
  addproForm: FormGroup;
 constructor(private formBuilder: FormBuilder,
   private  router:  Router, 
   private service:UserService
   // private auth: AuthenticationService
    ) { }
    ngOnInit() {
      this.addproForm = this.formBuilder.group({
        name: ['', Validators.required],// you can pass value as well bydefoult i doing it blank
        price: ['', Validators.required],
        code: ['', Validators.required],
        
      });  
    }
  get name() {
    return this.addproForm.get("name");
  }
  get price() {
    return this.addproForm.get("price");
  }
   get code() {
    return this.addproForm.get("code");
  } 
 
  add(){
    var details = {
      'name':  this.name.value,
      'code':this.code.value,
      'price':this.price.value
    }
   this.service.addproduct(details)
  }

}
