import { Component, OnInit, ViewChild } from '@angular/core';
import { addProductComponent } from '../addProduct/addProduct.component';
import { MatTableDataSource } from "@angular/material/table";


import { MatPaginator } from "@angular/material/paginator";
import { UserService } from '../../user.service';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit{
  constructor(private service:UserService){}
   dataSource: MatTableDataSource<any>;
   displayedColumns = ["code","name", "price"];
   @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ngOnInit() {
    this.product();
  }
  res:any;
product(){
   this.res = this.service.getproduct()
   this.dataSource = new MatTableDataSource(this.res); 
   this.dataSource.paginator = this.paginator;
}
 public doFilter = (value: string) => {
   this.dataSource.filter = value.trim().toLocaleLowerCase();
 };
}
export interface PeriodicElement {
  name: string;
  img: string;
  code: number;
  price:string;
  active : boolean;
  id:number;
}
