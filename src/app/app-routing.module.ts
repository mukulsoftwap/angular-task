import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component'         
//import { AuthGuardService } from './services/auth.guard.service';

const routes: Routes = [
  { path: "", component: LoginComponent },
  {path:'user', loadChildren : "./user/user.module#userModule"}
  // { path: "**", redirectTo: "" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    scrollPositionRestoration: "enabled",
    useHash: false
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
//canActivate: [AuthGuardService],canActivateChild: [AuthGuardService] 