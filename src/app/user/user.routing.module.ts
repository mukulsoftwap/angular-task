import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UserComponent } from "./user.component";
import { ProductComponent } from './components/product/product.component';
import { addProductComponent } from './components/addProduct/addProduct.component';
import { DashboardComponent } from "./components/dashboard/dashboard.component";
// import { AuthGuardService } from "../services/auth.guard.service";

const routes: Routes = [
  {
    path: "",
    component: UserComponent,
    // canActivate: [AuthGuardService],
    // canActivateChild: [AuthGuardService],
    children: [
      { path: "", redirectTo: "dashboard", pathMatch: "full" },
      { path: "dashboard", component: DashboardComponent },
      { path: "product", component: ProductComponent },
      { path: "ProductAdd", component: addProductComponent }

    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class userRoutingModule {}

export const routedComponents = [
    ProductComponent,addProductComponent,DashboardComponent
];
