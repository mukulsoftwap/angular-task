import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { userRoutingModule,routedComponents,} from "./user.routing.module";
import { UserComponent } from './user.component';

import { MatSliderModule } from "@angular/material/slider";
import { MatSidenavModule } from "@angular/material/sidenav";
import {MatExpansionModule, MatAccordion,MatExpansionPanel} from "@angular/material/expansion";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule, MatProgressBarModule } from "@angular/material";
import { MatSelectModule } from "@angular/material/select";
import { MatMenuModule } from "@angular/material/menu";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatTableModule } from "@angular/material/table";
import { MatIconModule } from "@angular/material/icon";
import { MatPaginatorModule } from "@angular/material/paginator";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    routedComponents,
    UserComponent
  ],
  imports: [
    CommonModule,
    userRoutingModule,
    MatSliderModule,
    MatSidenavModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatMenuModule,
    MatToolbarModule,
    MatGridListModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatProgressBarModule,
    MatIconModule,
    // ReactiveFormsModule,
    MatPaginatorModule,
    FormsModule,
    ReactiveFormsModule
  ],
  
  providers: [],
})
export class userModule  { 
  constructor(){
    console.log('userModule module');
  }
}
