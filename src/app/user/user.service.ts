import { Injectable, EventEmitter } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class UserService {
  base_url = "http://13.235.244.65:1337/api/v1/"; // "http://13.235.245.2:1337/";
  product =[
    {'name':'product 1','code' : 'dummy','price' : '10'},
    {'name':'product 2','code' : 'dummy','price' : '20'},
    // {'name':'product 3','code' : 'dummy','price' : '30',"id":3},
    // {'name':'product 4','code' : 'dummy','price' : '40',"id":4},
    // {'name':'product 5','code' : 'dummy','price' : '69',"id":5},
    // {'name':'product 6','code' : 'dummy','price' : '89',"id":6},
    // {'name':'product 7','code' : 'dummy','price' : '99',"id":7}
  ]
 
  constructor() {}
  addproduct(data){
   this.product.push(data)
   alert('Add product successfully :)')
  }
  getproduct(){
    return this.product;
  }
}