import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import {AuthenticationService} from '../services/Authentication.Service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private  router:  Router, 
     private auth: AuthenticationService
     ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required],// you can pass value as well bydefoult i doing it blank
      email: ['', Validators.required],
      password: ['', Validators.required]
    });  
  }
  get name() {
    return this.loginForm.get("name");
  }
  get email() {
    return this.loginForm.get("email");
  } 
  get password() {
    return this.loginForm.get("password");
  } 
  check
  onSubmit(){
  // let formdata = new FormData();
  // formdata.append("name", this.name.value);
  // formdata.append("email", this.email.value);
  // formdata.append("active",this.password.value );
  var details = {
    'name':  this.name.value,
    'email':this.email.value,
    'password':this.password.value,
  }
 this.check = this.auth.login(details)
   if(this.check){
     alert('login successfully :)')
     this.router.navigate(["/user"]);

   }
 
// if(details){
//   if(details.name == "pinki" && details.password == "123456"){
//     localStorage.setItem('token', '123456');
//   }else{
//     alert('error')
//   } 
// }
 }

}
