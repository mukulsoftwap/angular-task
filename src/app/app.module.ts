import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
// 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule, MatMenuModule, MatIconModule,MatButtonModule,  MatTableModule, MatInputModule,MatCardModule,MatSelectModule, MatOptionModule} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AuthenticationService} from './services/Authentication.Service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
      BrowserModule,
      AppRoutingModule,
      BrowserModule,
      AppRoutingModule,
      FlexLayoutModule,
      FormsModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      MatToolbarModule,
      MatInputModule,
      MatCardModule,
      MatMenuModule,
      MatIconModule,
      MatButtonModule,
      MatTableModule,
      MatSelectModule,
      MatOptionModule,
  ],
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
