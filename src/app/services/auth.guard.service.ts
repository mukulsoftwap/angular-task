import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
@Injectable({ providedIn: "root" })
export class AuthGuardService implements CanActivate, CanActivateChild {
  constructor(
    private router: Router
  ) {}

  canActivate(): boolean {
    if (!localStorage.getItem('currentUser') || !localStorage.getItem('token')) {
     // this.router.navigate(["login"]);
      return false;
    }
    return true;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    nextState: RouterStateSnapshot
  ): boolean {
    if (!localStorage.getItem('currentUser') || !localStorage.getItem('token')) {
     // this.router.navigate(["auth"]);
      return false;
    }
    return true;
  }

}
